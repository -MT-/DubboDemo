package com.dubbo.demo.remote.inter;

import com.dubbo.demo.remote.dto.User;

public interface IDubboService {

	User getByUserId(int id);

	boolean addUser(User u);
}
