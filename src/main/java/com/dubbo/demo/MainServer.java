package com.dubbo.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainServer {
	private final static Logger log = LoggerFactory.getLogger(MainServer.class);
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		log.info("SpringContext加载...");
		new ClassPathXmlApplicationContext(new String[] { "spring.xml" }).start();
		log.info("Spring is runing");
		while (true) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
