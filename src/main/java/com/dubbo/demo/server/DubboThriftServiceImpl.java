package com.dubbo.demo.server;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.rpc.RpcContext;
import com.alibaba.fastjson.JSON;
import com.dubbo.demo.remote.dto.ResultDto;
import com.dubbo.demo.remote.dto.UserThriftDto;
import com.dubbo.demo.remote.inter.IDubboThriftService;

@Component("dubboThriftServiceImpl")
public class DubboThriftServiceImpl implements IDubboThriftService.Iface {
	Map<Integer, UserThriftDto> umap = new HashMap<Integer, UserThriftDto>();

	@Override
	public UserThriftDto getByUserId(int id) {
		RpcContext rpcc = RpcContext.getContext();
		String clientIP = rpcc.getRemoteHost();
		System.out.println("来自【" + clientIP + "】的发起了一个请求," + JSON.toJSONString(rpcc.getUrl()));
		UserThriftDto u = umap.get(id);
		return u;
	}

	@Override
	public ResultDto addUser(UserThriftDto u,String tanceId) {
		ResultDto dto = new ResultDto(1, "成功");
		umap.put(u.getId(), u);
		return dto;
	}
}
