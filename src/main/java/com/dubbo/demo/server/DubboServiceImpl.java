package com.dubbo.demo.server;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.rpc.RpcContext;
import com.alibaba.fastjson.JSON;
import com.dubbo.demo.remote.dto.User;
import com.dubbo.demo.remote.inter.IDubboService;

@Component("dubboServiceImpl")
public class DubboServiceImpl implements IDubboService {
	Map<Integer, User> umap = new HashMap<Integer, User>();

	@Override
	public User getByUserId(int id) {
		RpcContext rpcc = RpcContext.getContext();
		String clientIP = rpcc.getRemoteHost();
		System.out.println("来自【" + clientIP + "】的发起了一个请求," + JSON.toJSONString(rpcc.getUrl()));
		User u = umap.get(id);
		return u;
	}

	@Override
	public boolean addUser(User u) {
		umap.put(u.getId(), u);
		return true;
	}
}
