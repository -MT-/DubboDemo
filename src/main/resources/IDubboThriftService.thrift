﻿//bool，i8，i16，i32，i64，double，string，binary，map<t1,t2>，list<t1>，set<t1>
//thrift-0.9.3.exe -r -gen java IDubboThriftService.thrift
struct ResultDto{
	1:i32 code;
	2:string msg;
}

struct UserThriftDto  {
 1: i32 id;
 2: string login_name; 
 3: string login_pwd; 
}

service IDubboThriftService{
	UserThriftDto getByUserId(1:i32 id);
	ResultDto addUser(1:UserThriftDto dto,2:string tanceId);
}