package client;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.dubbo.demo.remote.dto.User;
import com.dubbo.demo.remote.inter.IDubboService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-dubbo.xml")
public class DubboClient extends AbstractJUnit4SpringContextTests {

	@Autowired
//	@Reference(version="0.01",group="AnnoDubbo")
	private IDubboService dubboServer;

	@Test
	public void getUserByID() {
		int i = 1000;
		while (true) {
			++i;
			User u = new User(i, "用户名" + i, "密码" + i);
			dubboServer.addUser(u);
			System.out.println("插入：" + JSON.toJSONString(u));
			User su = dubboServer.getByUserId(i - 5);
			System.out.println("获取[" + (i - 5) + "]:" + JSON.toJSONString(su));
			try {
				Thread.sleep(15000);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

}
