package client.Asyn;

/**
 * dubbo 客户端异步
 * 
 * @author Yuanqy
 *
 */
public interface Notify {

	/**
	 * 异步回调
	 * 
	 * @param ret
	 *            回调
	 * @param req
	 *            入参
	 */
	public <T> void onReturn(T ret, Object... req);

	/**
	 * 异步回调异常
	 * 
	 * @param ex
	 *            异常
	 * @param req
	 *            入参
	 */
	public void onThrow(Throwable ex, Object... req);

}
