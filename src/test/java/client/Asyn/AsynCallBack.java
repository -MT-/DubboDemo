package client.Asyn;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

@Component("asynCallBack")
public class AsynCallBack implements Notify {

	@Override
	public <T> void onReturn(T ret, Object... req) {
		System.err.println("【异步回调】:\nREQ:" + JSON.toJSONString(req) + "\nRES:" + JSON.toJSONString(ret));
	}

	@Override
	public void onThrow(Throwable ex, Object... req) {
		System.err.println("【异步异常】:\nREQ:" + JSON.toJSONString(req));
		ex.printStackTrace();
	}
}
