package client;

import org.apache.thrift.TException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.dubbo.demo.remote.dto.User;
import com.dubbo.demo.remote.dto.UserThriftDto;
import com.dubbo.demo.remote.inter.IDubboService;
import com.dubbo.demo.remote.inter.IDubboThriftService;
import com.dubbo.demo.util.UUIDUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-Asyndubbo.xml")
public class DubboThriftAsynClient extends AbstractJUnit4SpringContextTests {

	@Autowired
	// @Reference(version="0.01",group="AnnoDubbo")
	private IDubboThriftService.Iface dubboServer;

	@Test
	public void getUserByID() throws TException {
		int i = 1000;
		while (true) {
			++i;
			UserThriftDto u = new UserThriftDto(i, "用户名" + i, "密码" + i);
			dubboServer.addUser(u,UUIDUtil.getUUID());
			System.out.println("插入：" + JSON.toJSONString(u));
			try {
				Thread.sleep(15000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
